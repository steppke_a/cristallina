from slic.core.adjustable import Adjustable


class MyNewCoolThing(Adjustable):

    pos = 0

    def get_current_value(self):
        return self.pos

    def set_target_value(self, value):
        self.pos = value

    def is_moving(self):
        return False  # OK OK, this is probably cheating ;)
