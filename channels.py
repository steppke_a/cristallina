# Channels at Cristallina endstation


##########################################################################################################
# BS channels

# TODO: JF settings regarding raw conversion, compression, etc.
detectors = [
    # "JF16T03V01",
]

####################
channels_gas_monitor = [
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG",
    "SARFE10-PBPG050:SLOW-X",
    "SARFE10-PBPG050:SLOW-Y",
    "SARFE10-PBIG050-EVR0:CALCI",  # good for correlations with total beam intensity
    "SARFE10-PBPG050:HAMP-INTENSITY-CAL",
]

######################
### PCO edge camera for the wavefront analysis
channels_PCO = [
    "SARES30-CAMS156-PCO1:FPICTURE",
]

######################
### SwissMX OAV camera picture
channels_OAV = []  # "SARES30-CAMS156-SMX-OAV:FPICTURE",

######################
### PBPS053
channels_PBPS053 = [
    "SARFE10-PBPS053:INTENSITY",
    "SARFE10-PBPS053:XPOS",
    "SARFE10-PBPS053:YPOS",
]

###################################
### Beam position monitor PBPS113
channels_PBPS113 = [
    "SAROP31-PBPS113:INTENSITY",
    "SAROP31-PBPS113:INTENSITY_UJ",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD0",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD1",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD2",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD3",
    "SAROP31-PBPS113:Lnk9Ch0-PP_VAL_PD4",
    "SAROP31-PBPS113:XPOS",
    "SAROP31-PBPS113:YPOS",
]


####################
## PSS059
channels_pss059 = [
    # "SARFE10-PSSS059:FPICTURE",                           # full pictures only when really needed
    "SARFE10-PSSS059:SPECTRUM_X",
    "SARFE10-PSSS059:SPECTRUM_Y",
    "SARFE10-PSSS059:SPECTRUM_CENTER",
    "SARFE10-PSSS059:SPECTRUM_COM",
    "SARFE10-PSSS059:SPECTRUM_FWHM",
    "SARFE10-PSSS059:SPECTRUM_STD",
    "SARFE10-PSSS059:FIT_ERR",
    "SARFE10-PSSS059:processing_parameters",
    # SARFE10-PSSS059:SPECTRUM_AVG_CENTER
    # SARFE10-PSSS059:SPECTRUM_AVG_FWHM
    # SARFE10-PSSS059:SPECTRUM_AVG_Y
]


#######################
# from _proc process
channels_PPRM113 = [
    "SAROP31-PPRM113:intensity",
    "SAROP31-PPRM113:x_center_of_mass",
    "SAROP31-PPRM113:x_fit_amplitude",
    "SAROP31-PPRM113:x_fit_mean",
    "SAROP31-PPRM113:x_fit_offset",
    "SAROP31-PPRM113:x_fit_standard_deviation",
    "SAROP31-PPRM113:x_fwhm",
    "SAROP31-PPRM113:x_profile",
    "SAROP31-PPRM113:x_rms",
    "SAROP31-PPRM113:y_center_of_mass",
    "SAROP31-PPRM113:y_fit_amplitude",
    "SAROP31-PPRM113:y_fit_mean",
    "SAROP31-PPRM113:y_fit_offset",
    "SAROP31-PPRM113:y_fit_standard_deviation",
    "SAROP31-PPRM113:y_fwhm",
    "SAROP31-PPRM113:y_profile",
    "SAROP31-PPRM113:y_rms",
    # "SAROP31-PPRM113:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]

#######################
# from _proc process
channels_PPRM150 = [
    "SAROP31-PPRM150:intensity",
    "SAROP31-PPRM150:x_center_of_mass",
    "SAROP31-PPRM150:x_fit_amplitude",
    "SAROP31-PPRM150:x_fit_mean",
    "SAROP31-PPRM150:x_fit_offset",
    "SAROP31-PPRM150:x_fit_standard_deviation",
    "SAROP31-PPRM150:x_fwhm",
    "SAROP31-PPRM150:x_profile",
    "SAROP31-PPRM150:x_rms",
    "SAROP31-PPRM150:y_center_of_mass",
    "SAROP31-PPRM150:y_fit_amplitude",
    "SAROP31-PPRM150:y_fit_mean",
    "SAROP31-PPRM150:y_fit_offset",
    "SAROP31-PPRM150:y_fit_standard_deviation",
    "SAROP31-PPRM150:y_fwhm",
    "SAROP31-PPRM150:y_profile",
    "SAROP31-PPRM150:y_rms",
    # "SAROP31-PPRM150:FPICTURE",                         # full pictures for debugging purposes at the moment, from _ib process
]

###########################
# Beam position monitor
channel_PBPS149 = [
    "SAROP31-PBPS149:INTENSITY",
    "SAROP31-PBPS149:INTENSITY_UJ",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD0",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD1",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD2",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD3",
    "SAROP31-PBPS149:Lnk9Ch0-PP_VAL_PD4",
    "SAROP31-PBPS149:XPOS",
    "SAROP31-PBPS149:YPOS",
]


further_channels = [
    ####################
    ## Digitizer
    # Integration limits
    "SARES30-LTIM01-EVR0:DUMMY_PV1_NBS",
    "SARES30-LTIM01-EVR0:DUMMY_PV2_NBS",
    "SARES30-LTIM01-EVR0:DUMMY_PV3_NBS",
    "SARES30-LTIM01-EVR0:DUMMY_PV4_NBS",
    # Signal-Background
    "SARES30-LSCP1-FNS:CH0:VAL_GET",
    # Waveform signal
    "SARES30-LSCP1-CRISTA1:CH0:1",
    # Waveform trigger
    "SARES30-LSCP1-CRISTA1:CH2:1",
    # Calculated intensity
    "SARES30-LTIM01-EVR0:CALCI",
]


channels = (
    channels_gas_monitor
    + channels_PCO
    + channels_OAV
    + channels_PBPS053
    + channels_pss059
    + channels_PPRM113
    + channels_PPRM150
    + channel_PBPS149
    + channel_PBPS149
    + further_channels
)

##########################################################################################################

pv_channels = [
    ####################
    ## OAPU044
    "SARFE10-OAPU044:MOTOR_X",
    "SARFE10-OAPU044:MOTOR_Y",
    "SARFE10-OAPU044:MOTOR_W",
    "SARFE10-OAPU044:MOTOR_H",
    ####################
    ## OATT053
    "SARFE10-OATT053:MOTOR_1",
    "SARFE10-OATT053:MOTOR_1.RBV",
    "SARFE10-OATT053:MOTOR_2",
    "SARFE10-OATT053:MOTOR_2.RBV",
    "SARFE10-OATT053:MOTOR_3",
    "SARFE10-OATT053:MOTOR_3.RBV",
    "SARFE10-OATT053:MOTOR_4",
    "SARFE10-OATT053:MOTOR_4.RBV",
    "SARFE10-OATT053:MOTOR_5",
    "SARFE10-OATT053:MOTOR_5.RBV",
    "SARFE10-OATT053:MOTOR_6",
    "SARFE10-OATT053:MOTOR_6.RBV",
    "SARFE10-OATT053:ENERGY",
    "SARFE10-OATT053:TRANS_SP",
    "SARFE10-OATT053:TRANS_RB",
    ####################
    ## OATA150
    "SAROP31-OATA150:MOTOR_1",
    "SAROP31-OATA150:MOTOR_2",
    "SAROP31-OATA150:MOTOR_3",
    "SAROP31-OATA150:MOTOR_4",
    "SAROP31-OATA150:MOTOR_5",
    "SAROP31-OATA150:MOTOR_6",
    ####################
    ## PSSS
    "SARFE10-PSSS059:MOTOR_Y3.VAL",
    "SARFE10-PSSS059:MOTOR_ROT_X3.VAL",
    "SARFE10-PSSS059:MOTOR_X5.VAL",
    "SARFE10-PSSS059:MOTOR_X3.VAL",
    ###########################
    # KB mirrors
    "SAROP31-OKBV153:W_X.RBV",
    "SAROP31-OKBV153:W_Y.RBV",
    "SAROP31-OKBV153:W_RX.RBV",
    "SAROP31-OKBV153:W_RY.RBV",
    "SAROP31-OKBV153:W_RZ.RBV",
    "SAROP31-OKBV153:BU.RBV",
    "SAROP31-OKBV153:BD.RBV",
    "SAROP31-OKBV153:TY1.RBV",
    "SAROP31-OKBV153:TY2.RBV",
    "SAROP31-OKBV153:TY3.RBV",
    "SAROP31-OKBV153:TX1.RBV",
    "SAROP31-OKBV153:TX2.RBV",
    "SAROP31-OKBH154:W_X.RBV",
    "SAROP31-OKBH154:W_Y.RBV",
    "SAROP31-OKBH154:W_RX.RBV",
    "SAROP31-OKBH154:W_RY.RBV",
    "SAROP31-OKBH154:W_RZ.RBV",
    "SAROP31-OKBH154:BU.RBV",
    "SAROP31-OKBH154:BD.RBV",
    "SAROP31-OKBH154:TY1.RBV",
    "SAROP31-OKBH154:TY2.RBV",
    "SAROP31-OKBH154:TY3.RBV",
    "SAROP31-OKBH154:TX2.RBV",
    ####################
    ## FEL Photon Energy
    "SARUN:FELPHOTENE",
    ###################
    ## FEL Photon Pulse Energy
    "SARFE10-PBPG050:PHOTON-ENERGY-PER-PULSE-AVG",
]

pvs_slits = [
    # TODO: PVS slits can't be read by the DAQ module currently. Therefore disabled it.
    #    "SARFE10-OAPU044:MOTOR_X.VAL",
    #    "SARFE10-OAPU044:MOTOR_X.RBV"
]

pvs_apertures = [
    "SAROP31-OAPU149:MOTOR_X.VAL",  # the x pos of the aperture
    "SAROP31-OAPU149:MOTOR_X.RBV",  # the x pos of the aperture
]

###############################
smaract_channels = [
    "SARES30-XSMA156:X:MOTRBV",
    "SARES30-XSMA156:Y:MOTRBV",
    "SARES30-XSMA156:Z:MOTRBV",
    "SARES30-XSMA156:Ry:MOTRBV",
    "SARES30-XSMA156:Rx:MOTRBV",
    "SARES30-XSMA156:Rz:MOTRBV",
]

pvs = pvs_slits + pv_channels + smaract_channels
