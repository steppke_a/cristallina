from slic.core.adjustable import PVAdjustable, PVEnumAdjustable
from slic.devices.simpledevice import SimpleDevice
from slic.utils import as_shortcut


class PVStringAdjustable(PVAdjustable):
    def get_current_value(self):
        return self.pvs.readback.get(as_string=True).strip()



n_unds = [
    6, 7, 8, 9, 10, 11, 12, 13, # 14 is the CHIC
    15, 16, 17, 18, 19, 20, 21, 22
]

undulator_info = {}
for i in n_unds:
    undulator_info[f"energy{i}"]       = PVAdjustable(f"SATUN{i:02}-UIND030:FELPHOTENE",  internal=True)
    undulator_info[f"polarisation{i}"] = PVEnumAdjustable(f"SATUN{i:02}-UIND030:POL-SET", internal=True)



overview = SimpleDevice("Maloja Overview",
#    standa = standa,
#    exp_delay = exp_delay,
#    laser_delay = laser_delay,
#    LXT = lxt,

    FELrepRate         = PVAdjustable("SWISSFEL-STATUS:Bunch-2-Appl-Freq-RB", internal=True),
    PaddleChamber1x    = PVAdjustable("SATES21-XSMA166:MOT1:MOTRBV", internal=True),
    PaddleChamber1y    = PVAdjustable("SATES21-XSMA166:MOT2:MOTRBV", internal=True),
    PaddleChamber1z    = PVAdjustable("SATES21-XSMA166:MOT3:MOTRBV", internal=True),
    SmartActTTx        = PVAdjustable("SATES22-XSMA168:MOT10:MOTRBV", internal=True),
    SmartActTTy        = PVAdjustable("SATES22-XSMA168:MOT11:MOTRBV", internal=True),
    SmartActTTz        = PVAdjustable("SATES22-XSMA168:MOT12:MOTRBV", internal=True),
    ToFV1m             = PVAdjustable("SATES21-XSHV166:V-1-S-CH0", internal=True),
    ToFV1p             = PVAdjustable("SATES21-XSHV166:V-0-S-CH0", internal=True),
    ToFV2m             = PVAdjustable("SATES21-XSHV166:V-1-S-CH1", internal=True),
    ToFV2p             = PVAdjustable("SATES21-XSHV166:V-0-S-CH1", internal=True),
    ToFV3m             = PVAdjustable("SATES21-XSHV166:V-1-S-CH2", internal=True),
    ToFV3p             = PVAdjustable("SATES21-XSHV166:V-0-S-CH2", internal=True),
#    energy1            = PVAdjustable("SATUN06-UIND030:FELPHOTENE", internal=True),
#    energy2            = PVAdjustable("SATUN15-UIND030:FELPHOTENE", internal=True),
    manip2needleESx    = PVAdjustable("SATES20-MANIP2:MOTOR_1.VAL", internal=True),
    manip2needleESy    = PVAdjustable("SATES20-MANIP2:MOTOR_2.VAL", internal=True),
    manip2needleESz    = PVAdjustable("SATES20-MANIP2:MOTOR_3.VAL", internal=True),
#    pol1               = PVEnumAdjustable("SATUN06-UIND030:POL-SET", internal=True),
#    pol2               = PVEnumAdjustable("SATUN15-UIND030:POL-SET", internal=True),
    pressChamb2        = PVAdjustable("SATES21-VM-VT2020:PRESSURE", internal=True),
    pressChamb3        = PVAdjustable("SATES21-VM-VT3010:PRESSURE", internal=True),
    pressChamb3GasCell = PVAdjustable("SATES21-VM-VT3030:PRESSURE", internal=True),
    pulse_energy       = PVAdjustable("SATFE10-PEPG046:PHOTON-ENERGY-PER-PULSE-AVG", internal=True),
    timeStamp          = PVAdjustable("SF-CPCL-TIM:TIME", internal=True),

    chicane_current_rb = PVAdjustable("SATUN14-MBND100:I-READ", internal=True),
    chicane_current_sv = PVAdjustable("SATUN14-MBND100:I-SET", internal=True),

    att64              = PVStringAdjustable("SATFE10-OATT064:MOT2TRANS.VALD", internal=True),
    att65              = PVStringAdjustable("SATFE10-OATT065:MOT2TRANS.VALD", internal=True),

    **undulator_info
)



spreadsheet_line = [
    "timeStamp", "File name", 
    "Gas cell / TOF", 
    "standa","Sample", 
    "pressChamb3", 
    "pressChamb3GasCell", "Static/scan", "Scan parameter", "ScanStep", "shots", "Comments", "Two colors (Y/N)", "energy1", 
    "polarisation10", "energy2", 
    "polarisation19",
    "pulse_energy", 
    "chicane_current_rb", 
    "FELrepRate", 
    "att64", 
    "att65", "Grating", "order", "Slit", "Detector position X", "Detector position Y", "Detector position (angle)","Ek", "Ep", "Slit", "Mode", 
    "pressChamb2", "Gas", 
    "manip2needleESx", 
    "manip2needleESy", 
    "manip2needleESz", 
    "ToFV1p", 
    "ToFV2p", 
    "ToFV3p", 
    "ToFV1m", 
    "ToFV2m", 
    "ToFV3m", 
    "PaddleChamber1x", 
    "PaddleChamber1y", 
    "PaddleChamber1z", 
    "energy6", 
    "energy7", 
    "energy8", 
    "energy9", 
    "energy10", 
    "energy11", 
    "energy12", 
    "energy13", 
    "energy14", 
    "energy15", 
    "energy16", 
    "energy17", 
    "energy18", 
    "energy19", 
    "energy20", 
    "energy21", 
    "energy22", 
]



@as_shortcut
def print_overview():
    print(overview)

@as_shortcut
def print_line_for_spreadsheet():
    ov = overview.__dict__
    def get(i):
        if i in ov:
            return str(ov[i].get())
        return ""
    res = [get(i) for i in spreadsheet_line]
    res = ",".join(res)
    print(res)



