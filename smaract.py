from slic.devices.general.smaract import SmarActStage

# this currently uses a modified SmarActStage module
# otherwise the wait times are not working correctly.

smaract = SmarActStage("SARES30-XSMA156",
                       X='SARES30-XSMA156:X',
                       Y='SARES30-XSMA156:Y', 
                       Z='SARES30-XSMA156:Z', 
                       Ry='SARES30-XSMA156:Ry',
                       Rx='SARES30-XSMA156:Rx',
                       Rz='SARES30-XSMA156:Rz',
                       )

