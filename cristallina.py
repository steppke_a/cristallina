#!/usr/bin/env python

import logging

logger = logging.getLogger("slic")
logger.setLevel(logging.INFO)
logger.info("Loading started.")

from time import sleep
from datetime import datetime
import numpy as np

# from tqdm import trange
from epics import PV

from slic.gui import GUI
from slic.core.adjustable import Adjustable, PVAdjustable, DummyAdjustable
from slic.core.acquisition import SFAcquisition, PVAcquisition
from slic.core.condition import PVCondition
from slic.core.scanner import Scanner
from slic.devices.simpledevice import SimpleDevice
from slic.devices.general.motor import Motor
from slic.utils import devices, Marker, as_shortcut
from slic.utils import Channels, Config, Elog, Screenshot, PV
from slic.core.acquisition.fakeacquisition import FakeAcquisition

# from devices.attenuator import Attenuator

from channels import detectors, channels, pvs
from spreadsheet import overview

# DEVICES
## example devices and adjustables
from cool_motor import MyNewCoolThing

cool_motor = MyNewCoolThing("cool_motor")

dummy = DummyAdjustable(units="au")

## Attenuator
from slic.devices.xoptics.attenuator_aramis import AttenuatorAramis
from knife_edge import KnifeEdge

attenuator_ID = "SAROP31-OATA150"
attenuator = AttenuatorAramis(
    attenuator_ID, description="Attenuators with absolute encoders"
)


def test_attenuator():
    tfundamental, tHG = attenuator.get_transmission(verbose=False)
    try:
        assert tfundamental > 0
    except TypeError:
        print("No transmission value reported from {attenuator.ID}")


test_attenuator()


## Undulator
import undulator

undulators = undulator.Undulators()


## Slits
from slic.devices.xoptics import slits

# _old for Alvra codepath, recommended here
slits_ADC = slits.SlitPosWidth_old("SAROP31-OAPU149", name="Apertures - ADC")

## Smaract stage
from smaract import smaract


###########################################
instrument = "cristallina"

# pgroup = "p19739" # commissioning March 2022 -- July 2022
pgroup = "p20443"  # commissioning Wavefront Sensor August 2022

daq = SFAcquisition(
    instrument,
    pgroup,
    default_channels=channels,
    default_pvs=pvs,
    default_detectors=detectors,
    rate_multiplicator=1,
)

# daq = FakeAcquisition(instrument, pgroup)
# daqPV = PVAcquisition(instrument, pgroup, default_channels=channels_ks) # workaround for KS not going to DB

# scan = Scanner(scan_info_dir=f"/sf/{instrument}/data/{pgroup}/res/scan_info", default_acquisitions=[daq], condition=None)

scan = Scanner(default_acquisitions=[daq])
gui = GUI(scan, show_goto=True, show_spec=True)

logger.info(f"Running at {instrument} with pgroup {pgroup}.")
logger.info("Loading finished.")
